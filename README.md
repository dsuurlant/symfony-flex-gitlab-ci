# symfony-flex-gitlab-ci

[![pipeline status](https://gitlab.com/dsuurlant/symfony-flex-gitlab-ci/badges/master/pipeline.svg)](https://gitlab.com/dsuurlant/symfony-flex-gitlab-ci/commits/master)

#### Kickstart your Dockerized SF3.3 + Flex project on GitLab with this simple setup.

This preview setup is currently based on Symfony 3.3 and Symfony Flex. When SF4 is released, this stack and its
setup may change.

You can read more about Symfony 4 and how to work with it in Fabien Potencier's SF4 series of articles, starting here:
 [Symfony 4: A New Way To Develop Applications.](https://symfony.com/blog/symfony-4-a-new-way-to-develop-applications)

This stack is composed of the following:

- php:7.1-fpm
- nginx on debian:jessie
- mariadb:10.3

Additional development tools included:

- XDebug
- PHPCS
- PHPUnit

### Installation

Download the .zip file and unpack in a directory of your choosing.

To get started, just run

```bash
docker-compose build
```

followed by

```bash
docker-compose push
```

and

```bash
docker-compose up -d
```

Then install dependencies with

```bash
docker exec -it -u www-data PHP_CONTAINER_NAME composer install \
    --optimize-autoloader --no-interaction --no-progress --no-scripts
```

You can run your PHPUnit tests through the app container:

```bash
docker exec -it -u www-data PHP_CONTAINER_NAME vendor/bin/phpunit
```

### GitLab integration

In order for the GitLab integration to work, connect to an existing repository or create a new one and point it to
a project that is hosted or mirrored on GitLab. Stage, commit and push the files and GitLab's own CI will pick up
on `.gitlab-ci.yml` and start running your build.

You can also run the tests locally by [installing GitLab Runner](https://docs.gitlab.com/runner/install/)
and running

```
gitlab-runner exec docker test
```

Keep in mind that `gitlab-runner exec` is deprecated and may be removed in newer versions of the Runner.

### Virtual host setup

You'll need to add your `docker-machine ip` address to `/etc/hosts` as well as the project's development domain name --
the default is 'symfony.dev'. Otherwise, the ip of your docker machine and the php container's port (default: 80)
works as well.

### XDebug setup

Setting up XDebug in a Docker container can be tricky. It should be good to go, but you need to configure it with your
own local network ip. On a Mac, this is `ipconfig getifaddr en0`.

Take a look at `xdebug.ini` and change the value of remote_host:

```bash
xdebug.remote_host = your.ip.goes.here
```

Run another `docker-compose build` to update the XDebug configuration.

You can check if it works through phpinfo:

```bash
docker exec -it -u www-data PHP_CONTAINER_NAME php -i | grep xdebug
```

The following file should come up: `/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini`

And the contents of that file should give you your latest XDebug configuration.

```bash
docker exec -it -u www-data PHP_CONTAINER_NAME cat /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
```

If you're using PHPStorm, you'll also need to tell your PHP container how to connect to it. It should work out of the
box through the line `PHP_IDE_CONFIG="serverName=symfony.dev` in `docker-compose.yml` but as of writing this README I
have not tested this yet.

### Finally

Issue reports and suggestions welcome, and feel free to fork and do your own thing with it.
