<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @author Danielle Suurlant <danielle.suurlant@gmail.com>
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @return Response
     */
    public function index()
    {
        return new Response("Index", Response::HTTP_OK);
    }
}
